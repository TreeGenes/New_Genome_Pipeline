/* Genomes WORKFLOW
* Slack  : https://join.slack.com/share/enQtNjcwNTU3Nzk0MzM1MS1mZDYwNTgwOTk2YTI5MDZjNTFhZGRjNmM2ODI1MTUyODhhMGJlODdmOWQ1OTg3ZTA5MjMzODE0NTY5YjIzZmM2
* GitHub : https://gitlab.com/TreeGenes/New_Genome_Pipeline/-/tree/master/ftp_scripts
* Maintainer: Gabriel Barrett
*/

//def summary_params = NfcoreSchema.paramsSummaryMap(workflow, params)
///////////////////////////////////////////////////////////////////////////////////////////
// valdiate input

WorkflowGenomes.initialise(params, log)

if (params.fasta) { ch_input = file(params.fasta) } else { exit 1, 'Need to specify path to genome file' }

// check input files exist
def checkPathParamList = [params.fasta, params.cfasta, params.gtf, params.gff, params.pfasta, params.plastid_fasta]
for (param in checkPathParamList) { if (param) if (param.isEmpty()) { exit 1, "file does not exist or is empty: ${param}" } }

/////////////////////////////////////////////////////////////////////////////////////////////
// nf-core modules
include { CUSTOM_DUMPSOFTWAREVERSIONS } from '../modules/nf-core/custom/dumpsoftwareversions/main.nf'
// local modules 
include { TREEGENES_CHECKGFFMATCHESFASTA } from '../modules/local/treegenes/checkgffmatchesfasta/main.nf'
include { README as README_ANNOTATION } from '../modules/local/readme/main.nf' // used to collect output stats and place in same README
include { README as CUSTOM_DUMPMD5SUM } from '../modules/local/readme/main.nf'

// local subworklfows
// genome
include { PREPARE_GENOME                                         } from '../subworkflows/local/new_genome/prepare_genomes'
include { FASTA_BLAST_BWA_GMAP_HISAT2 as FASTA_BUILDER           } from '../subworkflows/local/new_genome/fasta_blast_bwa_gmap_minimap2_hisat2_genome_index'
include { CFASTA_BLAST_QUAST as CFASTA_BUILDER                   } from '../subworkflows/local/new_genome/cfasta_blast_quast'
include { PFASTA_BLAST_DIAMOND_BUSCO as PFASTA_BUILDER           } from '../subworkflows/local/new_genome/pfasta_blast_diamond_busco'
include { TFASTA_GMAP_AGAT as TFASTA_BUILDER                     } from '../subworkflows/local/new_genome/tfasta_gmap_agat'
//transcriptome
include { TFASTA_TRANSDECODER_QUAST_BUSCO_DIAMOND as TOME        } from '../subworkflows/local/new_transcriptome/tfasta_transdecoder_quast_busco_diamond'

include { fromQuery } from 'plugin/nf-sqldb'

workflow GENOME_DATABASE_BUILDER {

    def hostname = WorkflowGenomes.getHostName(params, log)
    if (hostname == "treegenesprod" || hostname.contains("mantis")) {
        log.info(":) we are going to query psql")
        query = "select genus||' '||species as species from chado.organism where organism_id = (select organism_id from chado.organismprop where type_id = 52307 and value ilike '${params.species_code}');"
        channel.fromQuery(query, db: 'treegenes_db').view{"Query ${it}"}
    }

    ch_versions = Channel.empty()

    PREPARE_GENOME ()
    ch_versions = ch_versions.mix(PREPARE_GENOME.out.versions)

    if (['genomes', 'genome'].contains(params.workflow.toLowerCase())) {
        
        // this is setup solely for treegenes_checkgffmatchesfasta because inputs are optional
        fasta = Channel.from(params.fasta)
        opt_cfasta = !params.cfasta ? file("$projectDir/assets/NO_FILE", checkIfExists:true) : Channel.from(params.cfasta)
        opt_pfasta = !params.pfasta ? file("$projectDir/assets/NO_FILE_2", checkIfExists:true) : Channel.from(params.pfasta)
        opt_tfasta = !params.tfasta ? file("$projectDir/assets/NO_FILE_3", checkIfExists:true) : Channel.from(params.tfasta)
        
        if (params.checkIDs && !params.runNewGenomesPipeline) {

            TREEGENES_CHECKGFFMATCHESFASTA ( fasta, PREPARE_GENOME.out.gff.map{it[1]}, opt_cfasta, opt_pfasta, opt_tfasta )
            TREEGENES_CHECKGFFMATCHESFASTA.out.json.map { WorkflowGenomes.checkGff3MatchesFasta (it, log) }
        } else {
            
            if (params.checkIDs) {

                TREEGENES_CHECKGFFMATCHESFASTA ( fasta, PREPARE_GENOME.out.gff.map{it[1]}, opt_cfasta, opt_pfasta, opt_tfasta )
                TREEGENES_CHECKGFFMATCHESFASTA.out.json.map { WorkflowGenomes.checkGff3MatchesFasta (it, log) }
                
            } 

            // FASTA_BUILD
            FASTA_BUILDER ( PREPARE_GENOME.out.fasta ,  PREPARE_GENOME.out.gtf )
            ch_versions = ch_versions.mix(FASTA_BUILDER.out.versions)

            FASTA_BUILDER.out.quast.map { WorkflowGenomes.checkQuastStatistics(params, it, log, 'genome') }
            
            FASTA_BUILDER.out.busco.map { WorkflowGenomes.checkBuscoScore(params, it, log) } 

            // coding sequence fasta
            if ( params.cfasta ) { 
                CFASTA_BUILDER ( PREPARE_GENOME.out.cfasta , PREPARE_GENOME.out.fasta, PREPARE_GENOME.out.gtf )
                ch_versions = ch_versions.mix(CFASTA_BUILDER.out.versions)        
                
                CFASTA_BUILDER.out.quast.map { WorkflowGenomes.checkQuastStatistics(params, it[1], log, 'cds') } // check there are more than 10,000 genes at minimum
                }
            
            // create database based on protein sequences
            if ( params.pfasta ) { 
                PFASTA_BUILDER ( PREPARE_GENOME.out.pfasta, params.taxon ) 
                ch_versions = ch_versions.mix(PFASTA_BUILDER.out.versions)

                PFASTA_BUILDER.out.busco.map { WorkflowGenomes.checkBuscoScore(params, it, log) }
                }

            // Transcript fasta
            if ( params.tfasta ) { 
                TFASTA_BUILDER ( PREPARE_GENOME.out.tfasta , PREPARE_GENOME.out.fasta, PREPARE_GENOME.out.gff ) 
                ch_versions = ch_versions.mix(TFASTA_BUILDER.out.versions)
                }

            // Create README.md based on coding and protein sequences
            if ( params.cfasta && params.pfasta ) { README_ANNOTATION (Channel.value('annotations'), CFASTA_BUILDER.out.quast, PFASTA_BUILDER.out.busco, []) }    
        }
    
    } else {
        log.error (" Invalid workflow declaration workflow: ${params.workflow}")
        System.exit(1)
    }
    
    // write YAML versions
    CUSTOM_DUMPSOFTWAREVERSIONS (
        ch_versions.unique().collectFile(name: 'collated_versions.yml')
    )

    // write md5 sums
    CUSTOM_DUMPMD5SUM (
        'md5', [[],[]], [[:],[]], PREPARE_GENOME.out.md5sum.collectFile(name: 'collated_md5sums.md5')
        )
    
    WorkflowGenomes.changePermissions( workflow, launchDir, log )

}