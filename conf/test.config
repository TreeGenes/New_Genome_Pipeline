params {
    config_profile_name        = 'Test profile'
    config_profile_description = 'Minimal test dataset to check pipeline function'

    /*
    *    REQUIRED PARAMS
    */

    //fasta  = '/mnt/d/nextflow_testing/treegenes_database_builder/Abal/aalba5_s00000010.1_1.fa'
    fasta = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Abal/aalba5_s00000010.1_1.fa.gz'

    species_code = 'Alba'
    version      = 'v1.1'
    source       = 'user'

    gff    = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Abal/aalba5_s00000010.1_1.gff.gz'
    
    pfasta = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Abal/aalba5_s00000010.1_1.pep.fa.gz'
    cfasta = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Abal/aalba5_s00000010.1_1.cds.fa.gz'
    tfasta = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Abal/aalba5_s00000010.1_1.transcripts.fa.gz'

    haplotype = false

    source = 'user'

    /*******************************************************************************************************/

    /*
    test config for coordinates
    */
    study = 'snpchip'
    manifest = "${projectDir}/manifest_chip34K.csv"

    // pass additional files to QUAST used with cfasta
    quast_use_fasta = false
    quast_use_gtf = false

    // optionall file inputs BUSCO
    busco_lineages_path = []
    busco_config = []

    // disable EnTAP outside of xanadu
    enable_entap = false
    diamond_db   = "${projectDir}/bin/dummy.dmnd"
    taxon        = 'Abies'
    checkIDs     = true
    runNewGenomesPipeline = true

    outdir = './'

    // limit hisat2
    hisat2_build_memory = '1.GB'

    // Quast Checks
    min_total_length = '0'
    min_number_genes = '0'
    min_n50          = '0'

    max_memory = '2.GB'
    max_cpus   = 5
    max_time   = '50.min'

    create_dir    = false
    output_mode   = 'symlink'

    // saving intermediate files
    output_busco  = false
    output_quast  = false
    output_gunzip = false
    
}

process {
    cpus   = { check_max( 3    * task.attempt, 'cpus'   ) }
    memory = { check_max( 4.GB * task.attempt, 'memory' ) }
    time   = { check_max( 4.h  * task.attempt, 'time'   ) }
}