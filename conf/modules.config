// Directory Structure definitions
def source            = params.source.toString().toLowerCase().equals('user') ? 'GCA' : 'GCF'
def base_dir          = params.create_dir ? "${params.species_code}" + "/" + "${params.version}" : "./" + "${source}" + "/"
def haplotype         = params.haplotype ? ".${params.haplotype}" : ''
def version = params.version.toString()
if (version.contains("v")) {
    version = version.substring(1).replace('.', '_')
} else {
    version = version.replace('.', '_')
}

def base_name = "${params.species_code}.${version}${haplotype}"
def genome_index_dir  = base_dir + "/index/genome_index/" + base_name
def gene_index_dir    = base_dir + "/index/gene_index/" + base_name
def protein_index_dir = base_dir + "/index/protein_index/" + base_name

// must contain the transcript, peptide, gff, entap, cds
def annotation_dir    = base_dir + "/annotation/"
def alignments_dir    = base_dir + "/alignments/"
def genome_dir        = base_dir + "/genome/"
def cor_dir           = base_dir + "/.corrected/"
def plastid_mit_dir   = base_dir + "/mitochondrial/"
def plastid_chl_dir   = base_dir + "/chloroplast/"

//file(params.fasta).copyTo(genome_dir)

//def paramFileAnnotationList = [params.cfasta, params.pfasta, params.gff, params.gtf]
//for (param in paramFileAnnotationList) { if (param) if (!param.isEmpty()) {file(param).copyTo(annotation_dir)}}

process {

    // set default publishing 
    publishDir = [
        path: { "${params.outdir}/${task.process.tokenize(':')[-1].tokenize('_')[0]}" },
        mode: params.output_mode,
        saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
        enabled: false
    ]

    withName: 'GUNZIP_*' {
        ext.args = ''
        publishDir = [
            path: { "${params.outdir}/${task.process.tokenize(':')[-1].tokenize('_')[0]}" },
            mode: params.output_mode,
            enabled: params.output_gunzip,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }

    withName: '.*:PREPARE_GFASTA:TABIX_BGZIP' {
        ext.args = ''
        publishDir = [
            path: { genome_dir },
            mode: params.output_mode,
            enabled: true,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }
    
    withName: '.*:PREPARE_[PCT]FASTA:TABIX_BGZIP|.*:PREPARE_GFF:TABIX_BGZIP' {
        ext.args = ''
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            enabled: true,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: '*.gz'
        ]
    }

    withName: 'TREEGENES_CHECKGFFMATCHESFASTA' {
        publishDir = [
            [
                pattern: ".check*",
                path: { base_dir },
                mode: params.output_mode,
                enabled: true,
                saveAs: { filename -> filename.equals('version.yml') ? null : filename }
            ]
            /*[
                pattern: "cor*",
                path: { cor_dir },
                mode: params.output_mode,
                enabled: true,
                saveAs: { filename -> filename.equals('version.yml') ? null : filename }
            ]*/
        ]
    }

    withName: 'GFFREAD' {
        ext.args = ''
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename }  
        ]
    }

    withName: HISAT2_BUILD {
        ext.args = '--large-index'
        publishDir = [
            path: { genome_index_dir + '.hisat2'},
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*.ht2l"
        ]
    }

    withName: HISAT2_EXTRACTSPLICESITES {
        publishDir = [
            // add _annotation to this output
            path: { genome_index_dir + '.hisat2_annotation' },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            enabled: false
        ]
    }

    withName: BLAST_MAKEBLASTDB {
        ext.args = "-parse_seqids -dbtype nucl -out ${base_name}.fa"
        publishDir = [
            path: { genome_index_dir + '.genome_blast' },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*" 
        ]
    }

    withName: BLAST_MAKEBLASTDB_GENE {
        ext.args = "-parse_seqids -dbtype nucl -out ${base_name}.cds.fa"
        publishDir = [
            path: { gene_index_dir + '.gene_blast' },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename }
        ]    
    }

    withName: BLAST_MAKEBLASTDB_PROTEIN {
        ext.args = "-parse_seqids -dbtype prot -out ${base_name}.pep.fa"
        publishDir = [
            path: { protein_index_dir +  '.protein_blast'},
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]    
    }   
    withName: BWA_INDEX {
        publishDir = [
            path: { genome_index_dir + '.bwa' },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }

    withName: DIAMOND_MAKEDB {
        publishDir = [
            path: { protein_index_dir + '.protein_diamond' },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }

    withName: BUSCO {
        ext.args = { [ 
            params.public_database ? "-l ${params.public_database}" : '',
            '-m proteins -c 1'
        ].join(' ').trim() }
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            enabled: params.output_busco,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }
    withName: BUSCO_GENOME {
        ext.args = { [ 
            params.public_database ? "-l ${params.public_database}" : '',
            '-m genome -c 1'
        ].join(' ').trim() }
        publishDir = [
            path: { genome_dir },
            mode: params.output_mode,
            enabled: params.output_busco,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }
    withName: QUAST {
        ext.args = { [
            params.large_genome ? '--large' : '',
            ''
        ].join(' ').trim() }
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            enabled: params.output_quast,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename }             
        ]        
    }
    withName: QUAST_GENOME {
        ext.args = ''
        publishDir = [
            path: { genome_dir },
            mode: params.output_mode,
            enabled: params.output_quast,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename }            
        ]
    }
    withName: AGAT_CONVERTSPGXF2GXF {
        ext.args = ''
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '.gff' },
            pattern: '*.gff',
            enabled: false
        ]
    }
    withName: AGAT_CONVERTSPGFF2GTF {
        ext.args = ''
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '.gtf' },
            pattern: '*.gtf',
            enabled: false
        ]
    }
    withName: AGAT_SPSTATISTICS_ANN {
        ext.args = ''
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '.stats' } 
        ]
    }
    withName: AGAT_SPSTATISTICS_ALN {
        ext.args = ''
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '.minimap.stats' } 
        ]
    }
    withName: AGAT_GFF2BED {
        ext.args = ''
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            enabled: false
        ]
    }
    withName: MINIMAP2_INDEX {
        ext.args = ''
        publishDir = [
            path: { genome_index_dir + '.minimap2' },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename } 
        ]
    }
    withName: MINIMAP2_ALIGN {
        ext.args = '-ax splice -I 30G'
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            enabled: false
        ]
    }
    withName: BEDTOOLS_BAMTOBED {
        ext.args = ''
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            enabled: false
        ]
    }
    withName: AGAT_BED2GFF {
        ext.args = ''
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '.minimap.gff' }
        ]
    }
    withName: CUSTOM_DUMPSOFTWAREVERSIONS {
        ext.args = ''
        publishDir = [
            path: { base_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : '.software_versions.yml' },
            pattern: "software_versions.yml"
        ]
    }
    withName: README_GENOME {
        publishDir = [
            path: { genome_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*.txt"
        ]
    }
    withName: README_ALIGNMENTS {
        publishDir = [
            path: { alignments_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*.txt"
        ]
    }
    withName: README_ANNOTATION {
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*.txt"
        ]
    }
    withName: CUSTOM_DUMPMD5SUM {
        publishDir = [
            path: { base_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*.md5"
        ]
    }
    withName: ENTAP_CONFIG {
        publishDir = [
            path: { annotation_dir },
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "*.ini",
            enabled: false
        ]
    } 
    withName: ENTAP_RUNP {
        ext.args = ''
        publishDir = [
            [
                path: annotation_dir,
                mode: params.output_mode,
                saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '_full_entap.tsv' },
                pattern: "entap_results.tsv",
                enabled: true
            ],
            /*[
                path: annotation_dir,
                mode: params.output_mode,
                saveAs: { filename -> filename.equals('versions.yml') ? null : base_name + '_' + filename },
                pattern: "entap_outfiles/final_results/*.tsv",
                enabled: false
            ],
            [
                path: annotation_dir,
                mode: params.output_mode,
                saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
                enabled: false
            ]*/
        ]
    }
    /*withName: TRANSDECODER_LONGORF {
        ext.args = ''
    }
    withName: TRANSDECODER_PREDICT {
        ext.args = '--single_best_only'
    }
    withName: QUAST_TSA {
        ext.args = '--gene_mark'
    }
    withName: BUSCO_TSA {
        ext.args = '-m proteins'
    } 
    withName: WRITE_TSA_STATS {
        ext.args = ''
        publishDir = [
            path: 'tsa',
            mode: params.output_mode,
            saveAs: { filename -> filename.equals('versions.yml') ? null : filename },
            pattern: "README.txt"
        ]
    }*/
}

