params {
    config_profile_name        = 'Test profile'
    config_profile_description = 'Minimal test dataset to check gff ID checks function for ncbi'

    /*
    *    REQUIRED PARAMS
    */

    source = 'user'

    //fasta  = '/mnt/d/nextflow_testing/treegenes_database_builder/Abal/aalba5_s00000010.1_1.fa'
    fasta = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Algl/v1.1/genome/Algl.1_1.NC_084899.1.fna.gz'

    species_code = 'Algl'
    version      = 'v1.1'

    gff    = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Algl/v1.1/annotation/Algl.1_1.NC_084899.1.gff.gz'

    pfasta = null
    cfasta = 'https://gitlab.com/Gabriel-A-Barrett/testdata/-/raw/main/Algl/v1.1/annotation/Algl.1_1.NC_084899.1.NW_026909760.1.cds.fa.gz'
    tfasta = null

    haplotype = false

    /*******************************************************************************************************/

    // pass additional files to QUAST used with cfasta
    quast_use_fasta = false
    quast_use_gtf = false

    // optionall file inputs BUSCO
    busco_lineages_path = []
    busco_config = []

    // disable EnTAP outside of xanadu
    enable_entap = false
    diamond_db   = "${projectDir}/bin/dummy.dmnd"
    taxon        = 'Abies'
    checkIDs     = true
    runNewGenomesPipeline = false

    outdir = './'

    // limit hisat2
    hisat2_build_memory = '1.GB'

    // Quast Checks
    min_total_length = '0'
    min_number_genes = '0'
    min_n50          = '0'

    max_memory = '2.GB'
    max_cpus   = 5
    max_time   = '50.min'

    create_dir    = false
    output_mode   = 'symlink'

    // saving intermediate files
    output_busco  = false
    output_quast  = false
    output_gunzip = false
    
}

process {
    cpus   = { check_max( 3    * task.attempt, 'cpus'   ) }
    memory = { check_max( 4.GB * task.attempt, 'memory' ) }
    time   = { check_max( 4.h  * task.attempt, 'time'   ) }
}