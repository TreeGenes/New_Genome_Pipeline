[TOC]

# `TreeGenes/New_Genome_Pipeline`

A general pipeline for indexing, conducting sequence similarity searches, and summarising genome assemblies. Files are live in the **FTP directory** and are displayed to the user unless hidden with `.` in front of the file/directory name like for example `.genomes_temp/`. 

**FTP directory**: `/isg/treegenes/treegenes_store/FTP/Genomes`

<p align="center">
  <img src="media/workflow.png" alt="treegenes new genome assembly workflow">
</p>



## Four Letter Code Naming Scheme (*--species_code*)

Species are organized by four letter species code names, which are typically the first two letters of the species name combined with the first two letters of the genus name. For example, _Betula nana_ will become `Bena`. In some cases, if the four letter code is already in use by another species, the new species' code will become the first two letters of the genus name, combined with the second and third letters of the species name. In this case, _Abies balsamea_ becomes `Abal` since `Abba` is already taken. 

In order to keep track of these codes, there is an entry in the `organismprop` table for each species. Example query below:
```
select o.genus "Genus", o.species "Species", op.value "Code" from chado.organism o 
 join chado.organismprop op on o.organism_id=op.organism_id
 where op.type_id=52307;

      Genus      |    Species    | Code 
-----------------+---------------+-------
 Calocedrus      | decurrens     | Cade
 Cryptomeria     | japonica      | Crja
 Larix           | decidua       | Lade
 Larix           | kaempferi     | Laka
 ...
```
## Version Naming Scheme (*--version*)

Version numbers are based on the assignment provided by the authors. In the case that there is no version number than start with v1.0 and increment upwards. If there is a genome done on the *same* species but by *different* groups than make sure the more complete genome has the higher version number.

## Directory Structure

Assembly files are nested inside the four letter code and version. The full nucleotide genome assembly goes inside `genome` and annotated sequences should be placed inside the `annotation` folder for continuity. Files should be compressed with `gzip` to reduce disk space. 

```
Qumo
└── v1.0
    ├── annotation
    │   ├── Qumo.1_0.cds.fa.gz
    │   ├── Qumo.1_0.gff.gz
    │   ├── Qumo.1_0.pep.fa.gz
    └── genome
        └── Qumo.1_0.fa.gz
```
## Running `TreeGenes/New_Genome_Pipeline`

### Installation on Xanadu

1) If on xanadu add nextflow to $PATH

    `module load nextflow`

2) Install or update workflow if already installed:

   `nextflow pull -hub gitlab TreeGenes/New_Genome_Pipeline -r dev`

3) Change where temporary files get written
   
   `export NXF_WORK='/scratch/<username>/new_genome_pipeline'`

#### Minimum Usage

Below is an example of running the workflow on a hypothetical new genome Potr version 4.1.

`
nextflow run TreeGenes/New_Genome_Pipeline -r dev --species_code Potr --version v4.1 --fasta $(pwd)/Potr.4_1.fa --source 'user'
`

You are required to provide these arguments `-r` (gitlab branch pipeline is hosted),`--species_code` (four letter code),`--version` (expects v1.0),`--fasta` (full path to genome assembly nucleotide file).

### See [usage.md](./docs/usage.md) for additional information on parameters
### Full Usage

When running the pipeline on Xanadu it is good to submit it as a job to the cluster with nextflow run command using `SLURM`, write temporary files to a /scratch directory space `NXF_WORK`, and raise the temporary memory available to the java virtual machine `NXF_OPTS`. 

```bash
#!/bin/bash
#SBATCH --job-name=.newGenome
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load nextflow
export NXF_TEMP=/scratch/gbarrett # Change to your scratch directory
export NXF_WORK=/scratch/gbarrett # Change to your scratch directory
export NXF_OPTS="-Xms500M -Xmx9G" # set the range of memory on the Java virtual machine. Important for gff3 checks

nextflow run TreeGenes/New_Genome_Pipeline -r dev --species_code Potr --version v4.1\
 --fasta ../Potr/v4.1/genome/Potr.4_1.fa --gff ../Potr/v4.1/annotation/Potr.4_1.gff\
 --pfasta ../Potr/v4.1/annotation/Potr.4_1.pep.fa --cfasta ../Potr/v4.1/annotation/Potr.4_1.cds.fa\
 --taxon 'populus'\
 --create_dir=true\ # enables creating Potr/v4.1 or else with place genome,annotations,alignments,index in head directory
 -profile xanadu\
 -source user
```

### Checking GFF3 IDs

By default `New_Genome_Pipeline` verifies that the ID's specified in the gff match the corresponding fasta headers. The program only considers the first element of the fasta header and seperates by space (' ') or vertical bar ('|') and matches must be exact. It checks that the first column in the gff contains ID's in the genome assembly nucleotide fasta. For annotated sequences, the program checks for the presence of certain features (['gene', 'mRNA', 'CDS', 'transcript']) and looks for the presence of certain attributes (['ID', 'Name', 'product', 'pacid', 'protein_id', 'transcript_id']) to determine ID's matches between the gff3 and annotated peptide (pep), coding sequence (cds), and transcript (tsa) headers. Statistics are outputted to the console and written into `.checkGFF3matchesFASTA_summary.txt`.

if no mismatches are found between gff3 and fasta sequences then contents of `.checkGFF3matchesFASTA_summary.txt` will look like:

```
[checkGFF3MatchesFASTA] File: aalba5_s00000010.1_1.fa, Total IDs: 1, Mismatches: 0, feature: gene, attribute: genome_id
[checkGFF3MatchesFASTA] File: aalba5_s00000010.1_1.pep.fa, Total IDs: 1, Mismatches: 0, feature: transcript, attribute: product
[checkGFF3MatchesFASTA] File: aalba5_s00000010.1_1.cds.fa, Total IDs: 1, Mismatches: 0, feature: transcript, attribute: product
[checkGFF3MatchesFASTA] File: aalba5_s00000010.1_1.transcripts.fa, Total IDs: 1, Mismatches: 0, feature: transcript, attribute: ID
```

where Total IDs: <value> indicates the number of unique ID's found in the header, Mismatches: <value> indicates to the total number of gff3 ID's that did not match with the fasta ID's, feature: <item> provides information on which feature contained matching IDs and same with attribute: <item>. **IMPORTANT** if matches are NOT found then the full list of gff3 ID's will for feature: mRNA or gene and attribute: ID will be outputed. 

##### To exclusively run GFF3 checks disable the pipeline with `--runNewGenomePipeline false`

## Moving assembly in temp directory to LIVE
So the New_Genome_Pipeline completed successfully, gff IDs match and all the stats/indices were created... GREAT! To copy files to LIVE use `writeGenomesSummary.py`. This script will copy contents of genome, annotation, and index directories to the source directory provided by the user with the fourth argument. For example, `python3 writeGenomesSummary.py Qumo v1.0 /isg/treegenes/treegenes_store/FTP/Genomes` will copy files from the base directory `./Qumo/v1.0` into `/isg/treegenes/treegenes_store/FTP/Genomes/Qumo/v1.0`. This script will also remove any pre-existing, excluding fastas and supplemental directory see Line 85.

## Running [`looper.sh`](./looper.sh): Update references in directory

Recreates `./Genomes` directory with `TreeGenes/New_Genome_Pipeline` workflow and writes `./genomes_v2`

```
# To execute simply write in the command line 
bash-4.2$ sbatch looper.sh
```

The script `looper.sh` was created in the event that the whole genome directory space needs to be updated. This script will loop through the current directory space and expects pre-existing files to be located in `/isg/treegenes/treegenes_store/FTP/Genomes/*/*` and will output files into `/isg/treegenes/treegenes_store/FTP/genomes_v2` to subsequently be reviewed by the administrative team for overwriting `Genomes`.

**File** [genomeSummary.log](./genomeSummary.log), contains information on workflow completion status

### Cancel Jobs spawned by looper.sh

```
bash-4.2$ bash purge.sh
```

**IMPORTANT** `purge.sh` must be executed within the `/isg/treegenes/treegenes_store/FTP/genomes_v2` directory space or else it will remove desirable files. 

## File Permissions
All files in the FTP directory, for the purposes of reachability from nginx (aka the website) and to be able to be modified by lab members, **must** have the following permissions:

 - Group: `treegenes`
 - Permissions: `775` or `drwxrwxr-x` (directories) or `rwxrwxr-x` (regular files)

The file owner can be whoever, although it is preferred to be `tg-nginx`.

To set these permissions:
  - For files: `chgrp treegenes filename` & `chmod 775 filename`
  - For directories (recursive) `chgrp -R treegenes directory/` & `chmod -R 775 directory/`

If you're not sure which files you own,  you can get a list:
`find /isg/treegenes/treegenes_store/FTP/ -user $USER@cam.uchc.edu`
For more detailed information on the files you own:
`find /isg/treegenes/treegenes_store/FTP/ -user $USER@cam.uchc.edu -exec stat -c '%a %G %n' {} \;`

## Tripal Sequence Similarity Search (TESEQ)

TSeq (Tripal Sequence Similarity Search) provides users a list of available pre-indexed protein and nucleotide databases to run their DIAMOND or BLAST queries against.

Once the indexing stage is complete (see README.md), the database should be added to TSeq.
This can be done one of two ways:
1.  Filling out the form manually at `admin/tripal/extension/tseq/add_db`
2.  Importing a CSV sheet with 1 or more databases listed.

Required information is:
1. Type of database (Protein, Nucleotide, etc.)
2. Name of database (human readable, will show up in a dropdown list)
3. Version of database
4. Category (Most likely 'Standard', see TSeq documentation if using additional categories)
5. File location (Must be accessible on disk to the user who is running the command. Note that
        this refers to the user that Tripal Remote Job logs into as, assuming the job is being run on a remote machine)
6. Web Location (Optional) - If the original non-indexed file is available for download, list it here
7. Count - the number of sequences/scaffolds held in the database. This can be found by running `grep ">" file.fa | wc -l` on the original sequence file
