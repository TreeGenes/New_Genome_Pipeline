include { BLAST_MAKEBLASTDB         } from '../../../modules/nf-core/blast/makeblastdb/main.nf'
include { BWA_INDEX                 } from '../../../modules/nf-core/bwa/index/main'
include { HISAT2_BUILD              } from '../../../modules/nf-core/hisat2/build/main.nf'
include { HISAT2_EXTRACTSPLICESITES } from '../../../modules/nf-core/hisat2/extractsplicesites/main'
include { MINIMAP2_INDEX            } from '../../../modules/nf-core/minimap2/index/main'
include { QUAST as QUAST_GENOME     } from '../../../modules/nf-core/quast/main'
include { BUSCO as BUSCO_GENOME     } from '../../../modules/nf-core/busco/main'

// README modules
include { README as README_GENOME   } from '../../../modules/local/readme/main'

workflow FASTA_BLAST_BWA_GMAP_HISAT2 {
    
    take:
    fasta
    gtf

    main:
    ch_versions = Channel.empty()
    quast_path_version = Channel.empty()

    BLAST_MAKEBLASTDB ( fasta )
    ch_versions = ch_versions.mix(BLAST_MAKEBLASTDB.out.versions)

    BWA_INDEX ( fasta )
    ch_versions = ch_versions.mix(BWA_INDEX.out.versions)

    MINIMAP2_INDEX ( fasta )
    ch_versions = ch_versions.mix(MINIMAP2_INDEX.out.versions)

    // Add gFACs check (does fasta counts agree w/ annotation?) convert gffs to gtf when necessary
    if (params.gtf || params.gff) { 

        HISAT2_BUILD ( fasta, gtf.map{it[1]} , HISAT2_EXTRACTSPLICESITES ( fasta.map{it[1]}, gtf.map{it[1]} ).txt )
        ch_versions = ch_versions.mix(HISAT2_BUILD.out.versions)

    } else {
            
        HISAT2_BUILD ( fasta, [] , [] )
        ch_versions = ch_versions.mix(HISAT2_BUILD.out.versions)
    }

    QUAST_GENOME (fasta.map{it[1]}, [], [], params.quast_use_fasta, params.quast_use_gff)
    quast_path_version = quast_path_version.mix(QUAST_GENOME.out.versions.combine(QUAST_GENOME.out.tsv)) // add [path, val] to quast_version

    BUSCO_GENOME (fasta, params.busco_lineage, params.busco_lineages_path, params.busco_config)
    
    README_GENOME (Channel.value('genome'), quast_path_version, BUSCO_GENOME.out.short_summaries_txt, [])

    emit:
    minimap2_i = MINIMAP2_INDEX.out.index
    quast      = QUAST_GENOME.out.tsv
    busco      = BUSCO_GENOME.out.short_summaries_txt

    versions   = ch_versions

}