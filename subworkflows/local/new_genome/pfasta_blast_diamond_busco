include { DIAMOND_MAKEDB                                 } from '../../../modules/nf-core/diamond/makedb/main'
include { BLAST_MAKEBLASTDB as BLAST_MAKEBLASTDB_PROTEIN } from '../../../modules/nf-core/blast/makeblastdb/main'
include { BUSCO as BUSCO_PROTEIN } from '../../../modules/nf-core/busco/main'
include { ENTAP_CONFIG } from '../../../modules/local/entap/config/main.nf'
include { ENTAP_RUNP } from '../../../modules/local/entap/runp/main.nf'
include { fromQuery } from 'plugin/nf-sqldb'

workflow PFASTA_BLAST_DIAMOND_BUSCO {

    take:
    protein
    taxon

    main:
    ch_versions = Channel.empty()

    BLAST_MAKEBLASTDB_PROTEIN ( protein )
    ch_versions = ch_versions.mix(BLAST_MAKEBLASTDB_PROTEIN.out.versions)

    DIAMOND_MAKEDB ( protein.map{it[1]} )
    ch_versions = ch_versions.mix(DIAMOND_MAKEDB.out.versions)

    BUSCO_PROTEIN ( protein, params.busco_lineage, params.busco_lineages_path, params.busco_config )
    ch_versions = ch_versions.mix(BUSCO_PROTEIN.out.versions)

    if (params.enable_entap) {
        
        //query = "set search_path to chado; select genus from chado.organism where organism_id = (select organism_id from chado.organismprop where type_id = 52307 and value ilike ${params.species_code});"
        //channel.fromQuery(query, db: 'treegenes_db').view()
        ENTAP_CONFIG (taxon, params.tcoverage, params.qcoverage, params.entap_db, params.eggnog_db, params.data_eggnog, params.contam)
        ENTAP_RUNP (protein, ENTAP_CONFIG.out.config, params.diamond_db)
    }

    emit:
    busco    = BUSCO_PROTEIN.out.short_summaries_txt
    
    versions = ch_versions
}