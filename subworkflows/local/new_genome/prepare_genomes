include { GUNZIP as GUNZIP_FASTA   } from '../../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_GFF     } from '../../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_GTF     } from '../../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_CFASTA  } from '../../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_PFASTA  } from '../../../modules/nf-core/gunzip/main'
include { GUNZIP as GUNZIP_TFASTA  } from '../../../modules/nf-core/gunzip/main'
include { GFFREAD                  } from '../../../modules/nf-core/gffread/main'
include { AGAT_CONVERTSPGXF2GXF    } from '../../../modules/nf-core/agat/convertspgxf2gxf/main'
include { AGAT_CONVERTSPGFF2GTF    } from '../../../modules/nf-core/agat/convertspgff2gtf/main'
include { MD5SUM as GFASTA_MD5SUM  } from '../../../modules/nf-core/md5sum/main'
include { MD5SUM as PFASTA_MD5SUM  } from '../../../modules/nf-core/md5sum/main'
include { MD5SUM as CFASTA_MD5SUM  } from '../../../modules/nf-core/md5sum/main'
include { MD5SUM as TFASTA_MD5SUM  } from '../../../modules/nf-core/md5sum/main'
include { MD5SUM as GFF_MD5SUM     } from '../../../modules/nf-core/md5sum/main'
include { MD5SUM as GTF_MD5SUM     } from '../../../modules/nf-core/md5sum/main'
include { TABIX_BGZIP              } from '../../../modules/nf-core/tabix/bgzip/main'

include { FILE_MD5SUM_GUNZIP_BGZIP as PREPARE_GFASTA } from './file_md5sum_gunzip_bgzip'
include { FILE_MD5SUM_GUNZIP_BGZIP as PREPARE_GFF } from './file_md5sum_gunzip_bgzip'
include { FILE_MD5SUM_GUNZIP_BGZIP as PREPARE_GTF } from './file_md5sum_gunzip_bgzip'
include { FILE_MD5SUM_GUNZIP_BGZIP as PREPARE_PFASTA } from './file_md5sum_gunzip_bgzip'
include { FILE_MD5SUM_GUNZIP_BGZIP as PREPARE_TFASTA } from './file_md5sum_gunzip_bgzip'
include { FILE_MD5SUM_GUNZIP_BGZIP as PREPARE_CFASTA } from './file_md5sum_gunzip_bgzip'


workflow PREPARE_GENOME {

    /* 
    *   creates channels [[id:filename],file] 
    *   unzips files 
    *   calculates md5sum
    *   write bgzip file
    */

    ch_versions = Channel.empty() //slurped into yml
    ch_md5sum = Channel.empty() // slurped into md5 file
    
    def haplotype = params.haplotype ? ".${params.haplotype}" : ''
    def version = "${params.version.replaceAll("v","").replaceAll("\\.","_")}"
    def basename = "${params.species_code}" + "." + "${version}" + "${haplotype}"

    def endings = [".gz"]

    ch_fasta = channel.fromPath(params.fasta).map{[['id':basename, 'type':''], it]}
    ch_fasta = PREPARE_GFASTA ( ch_fasta , endings.any { params.fasta.endsWith(it) }).file // update to unziped version

    ch_md5sum = ch_md5sum.concat( PREPARE_GFASTA.out.md5sum.map{it[1]} )
    
    if (params.gff) {
        
        ch_gff = channel.fromPath(params.gff).map{[['id':basename, 'type':''], it]}
        
        cln_gff = AGAT_CONVERTSPGXF2GXF ( ch_gff ).output_gff
        ch_versions = ch_versions.mix(AGAT_CONVERTSPGXF2GXF.out.versions)

        ch_gff = PREPARE_GFF ( cln_gff, false ).file // unzip is false since it is coming unzipped from AGAT above
        ch_md5sum = ch_md5sum.concat( PREPARE_GFF.out.md5sum.map{it[1]} )

        // if there isn't a gtf file passed in params covert gff into gtf
        if (!params.gtf) { 
            ch_gtf = AGAT_CONVERTSPGFF2GTF( cln_gff ).output_gtf
            ch_versions = ch_versions.mix(AGAT_CONVERTSPGFF2GTF.out.versions)
        }
    
    } else {
        cln_gff = Channel.empty()
        ch_gff = Channel.empty()
    }


    if (params.gtf) {
        ch_gtf = Channel.fromPath(params.gtf).map{ [['id':basename, 'type':''], it ] }

        ch_gtf = PREPARE_GTF (ch_gtf, params.gtf.endsWith('.gz')).file
        ch_md5sum = ch_md5sum.concat( PREPARE_GTF.out.md5sum.map{it[1]} )

    } else if (!params.gtf && !params.gff) {
        ch_gtf = Channel.empty()
    }

    if (params.cfasta) {
        ch_cfasta = Channel.fromPath( params.cfasta ).map { [['id':basename, 'type':'.cds'], it] }
        
        ch_cfasta = PREPARE_CFASTA ( ch_cfasta, params.cfasta.endsWith('.gz')).file

        ch_md5sum = ch_md5sum.concat( PREPARE_CFASTA.out.md5sum.map{it[1]} )
    } else {
        ch_cfasta = Channel.empty()
    }

    if (params.pfasta) {
        ch_pfasta = Channel.fromPath(params.pfasta).map { [['id':basename, 'type':'.pep'], it] }

        ch_pfasta = PREPARE_PFASTA ( ch_pfasta, params.pfasta.endsWith('.gz') ).file

        ch_md5sum = ch_md5sum.concat( PREPARE_PFASTA.out.md5sum.map{it[1]} )
    } else {
        ch_pfasta = Channel.empty()

    }

    if (params.tfasta) {
        ch_tfasta = Channel.fromPath(params.tfasta).map {[['id':basename, 'type':'.transcripts'], it]}

        ch_tfasta = PREPARE_TFASTA ( ch_tfasta, params.tfasta.endsWith('.gz') ).file

        ch_md5sum = ch_md5sum.concat( PREPARE_TFASTA.out.md5sum.map{it[1]} )
    } else {
        ch_tfasta = Channel.empty()
    }


    emit:
    
    fasta  = ch_fasta
    raw_gff = ch_gff
    gff    = cln_gff
    gtf    = ch_gtf
    cfasta = ch_cfasta
    pfasta = ch_pfasta
    tfasta = ch_tfasta
    md5sum = ch_md5sum

    versions = ch_versions

}