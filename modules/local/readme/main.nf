process README {
    tag "$type"
    label 'process_low'

    conda "conda-forge::sed=4.7"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/ubuntu:20.04' :
        'quay.io/nf-core/ubuntu:20.04' }"

    input:
    val type
    tuple path(quast_version), path(quast)
    tuple val(meta), path(busco)
    path agat

    output:
    path '*.txt', emit: readme, optional: true
    path '*.md5', emit: md5, optional: true

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    def hap = params.haplotype ? '.' + params.haplotype : ''
    if (type == 'genome') {
        """
        (echo "########"; echo "QUAST Results"; echo "########"; echo "QUAST version is:" \$(cat ${quast_version} | grep 'quast:' | awk -F':' '{print \$2}'); echo "" ;cat $quast; echo ""; echo "########"; echo "BUSCO Results"; echo "########"; echo "" ;cat $busco) > README${hap}.txt
        """ 
    } else if (type == 'alignments') {
        """
        cat $agat > README.txt
        """
    } else if (type == 'annotations') {
        // TODO: change quast output to match 
        """
        (echo "########"; echo "QUAST Results"; echo "########";  echo "QUAST version is:" \$(cat ${quast_version} | grep 'quast:' | awk -F':' '{print \$2}'); echo "" ;cat $quast; echo ""; echo "########"; echo "BUSCO Results"; echo "########"; echo "" ;cat $busco) > README${hap}.txt
        # sed 's/# contigs (>= 0 bp)'
        """
    } else if (type == 'tsa') {
        """
        (echo "########"; echo "QUAST Results"; echo "########";  echo "QUAST version is:" \$(cat ${quast_version} | grep 'quast:' | awk -F':' '{print \$2}'); echo "" ;cat $quast; echo ""; echo "########"; echo "BUSCO Results"; echo "########"; echo "" ; cat $busco) > README${hap}.txt
        """
    } else if (type == 'md5') {
    """
    sort -n collated_md5sums.md5 | uniq > checksum${hap}.md5
    """
    } else { 
        exit(1)
    }
}

