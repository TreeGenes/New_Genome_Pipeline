process ENTAP_RUNP {
    tag "$protein"
    label 'process_super'

    //container "plantgenomics/easel:entap"
    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'plantgenomics/entap:v1.0.0':
        'plantgenomics/entap:v1.0.0' }"

    input:
    tuple val(meta), path(protein)
    path(config)
    path(database)

    output:
    path("entap_outfiles/*")                , emit: output
    path("entap_outfiles/final_results/*")  , emit: log
    path("entap_results.tsv")               , emit: final_message

    script:
    """
    # Main script starts here
    database_resolved_path=\$(readlink -f "${database}")
    EnTAP --runP --ini "${config}" -i "${protein}" -d "\${database_resolved_path}" --threads "${task.cpus}" --state 4x

    ln -s entap_outfiles/final_results/entap_results.tsv ./ 
    """
}
