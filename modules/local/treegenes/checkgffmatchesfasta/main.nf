process TREEGENES_CHECKGFFMATCHESFASTA {
    label 'process_small'

    container 'gbarrett9086/treegenes:latest'

    input:
    path fasta
    path gff
    path cfasta
    path pfasta
    path tfasta

    output:
    path(".checkGff3MatchesFasta.txt"), emit: summary
    path(".checkGff3Summary.json"), emit: json
    path("*.cor.*"), emit: corrected, optional: true

    when:
    task.ext.when == null || task.ext.when

    script:
    def args = task.ext.args ?: ''
    args += cfasta.name != 'NO_FILE' ? "-CFASTA ${cfasta} " : ''
    args += pfasta.name != 'NO_FILE_2' ? "-PFASTA ${pfasta} " : ''
    args += tfasta.name != 'NO_FILE_3' ? "-TFASTA ${tfasta} " : ''
    """
    treegenes \\
        CheckGffMatchesFasta \\
            -GFASTA $fasta \\
            -GFF $gff \\
            $args
    """
}
