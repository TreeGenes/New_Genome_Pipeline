import os
import time
import sys
import re
import pandas as pd
import subprocess
import shutil

def print_usage():
    print("Usage: python writeGenomesSummary.py <species_code> <version> <base_dir>")
    print("\tExample: python3 writeGenomesSummary.py Qumo v1.0 ./ /isg/treegenes/treegenes_store/FTP/Genomes")
    print("\tYou can optionally specify the fourth argument. If not provided defaults to Genomes")
    print("\tThis example creates a directory or write files into a pre-existing: /isg/treegenes/treegenes_store/FTP/Genomes/Qumo/v1.0")    
    sys.exit(1)

def checkIfCompletedSuccessfully(log_file):
    try:
        with open(log_file, 'r') as f:
            Completed_pattern = r'Completed at: (\S+)'
            for line in f:
                match = re.search(Completed_pattern, line)
                if match:
                    return True
            return False

    except FileNotFoundError:
        print("Log file not found. Exiting...")
        sys.exit(1)  # Exiting the script with an error code
    except Exception as e:
        print(f"An unexpected error occurred: {e}. Exiting...")
        sys.exit(1)  # Exiting the script with an error code for any other exception

def extract_duration(line):
    # Adjusted pattern to be more flexible with whitespace and case-insensitive
    duration_pattern = r'duration\s*:\s*((\d+d)?\s*(\d+h)?\s*(\d+m)?\s*(\d+s)?)'
    match = re.search(duration_pattern, line, re.IGNORECASE)  # Using re.IGNORECASE to make it case-insensitive
    if match:
        # Extract and format each time component, providing defaults if missing
        days = match.group(2) if match.group(2) else '0d'
        hours = match.group(3) if match.group(3) else '0h'
        minutes = match.group(4) if match.group(4) else '0m'
        seconds = match.group(5) if match.group(5) else '0s'
        duration = f"{days} {hours} {minutes} {seconds}".strip()
        return duration
    else:
        return False

def extract_module(line):
    module_pattern = r'\s+Process.*:(?!.*:)([^:\s]+)\s'
    match = re.search(module_pattern, line)
    if match:
        return match.group(1)
    else:
        return False

def extract_commanderror(line):
    commanderror_pattern = r'^Command error:\s*(.*)'

def copyFile(full_file_path, dest_file_path):
    success = True
    # Copy the file using subprocess.run
    result = subprocess.run(["cp", "-R", full_file_path, dest_file_path])
    
    # Check the return code
    if result.returncode == 0:
        print(f"Successfully copied {full_file_path} to {dest_file_path}")
    else:
        success = False
        print(f"Failed to copy {full_file_path} to {dest_file_path}")
    return success

def checkDirectoryExists(src_dir, dest_base_dir, genome_assembly_dir):
    dest_file_path_hidden = f"{dest_base_dir}/.{genome_assembly_dir}"
    dest_file_path = f"{dest_base_dir}/{genome_assembly_dir}"
    dest_path_dir = f"{dest_base_dir}/{genome_assembly_dir}"
    
    if os.path.exists(dest_file_path_hidden):
        print(f"hidden directory exists in {dest_file_path_hidden}")
        os.makedirs(dest_path_dir, exist_ok=True)
        success = operateOnFilesToDirectory(dest_file_path_hidden, dest_path_dir, set(['.nextflow.log', '.pipeline_params.yml', '.genomeLooper.out', '.genomeLooper.err', '.checkGFF3matchesFASTA_summary.txt', '.trace.txt', 'README.txt', '.nextflow', '.out', '.err', '.sh']), set(['.nextflow', 'index', 'busco', 'quast']), command='Copy')
        if success: 
            removeHiddenDirectory(f"{dest_base_dir}/.{genome_assembly_dir}")
    
    elif os.path.exists(dest_file_path):
        operateOnFilesToDirectory(src_dir=src_dir, dest_base_dir=dest_file_path, exclude_files=['.gz', '.fa', '.fna', '.faa', '.fasta', '.gff', '.gff3', '.gtf'], exclude_dirs = ['supplemental'], command = 'Remove')

    elif not os.path.exists(dest_path_dir):
        os.makedirs(dest_path_dir)
        print(f"Created directory: {dest_path_dir}")

def operateOnFilesToDirectory(src_dir, dest_base_dir, exclude_files = [], exclude_dirs = [], command = 'Copy'):
    """
    walk through genome assembly directory and copy files
    """
    success = False
    #exclude_dirs_list = set(['.nextflow',]) if nxf_dir else set(['.nextflow', 'index', 'busco', 'quast'])
    #exclude_files_list = set(['.nextflow.log', '.pipeline_params.yml', '.genomeLooper.out', '.genomeLooper.err', '.checkGFF3matchesFASTA_summary.txt', '.trace.txt']) if nxf_dir else set(['.nextflow.log', '.pipeline_params.yml', '.genomeLooper.out', '.genomeLooper.err', '.checkGFF3matchesFASTA_summary.txt', '.trace.txt', 'README.txt', '.nextflow', '.out', '.err', '.sh']) 
    for root, dirs, files in os.walk(src_dir):
        # excluding certain directories
        dirs[:] = [d for d in dirs if not d.startswith(tuple(exclude_dirs))]
        files[:] = [f for f in files if not f.endswith(tuple(exclude_files))]

        for filename in files:
            full_file_path = os.path.join(root, filename)
            dest_file_path = full_file_path.replace(src_dir, dest_base_dir, 1)
            dest_dir_path = os.path.dirname(dest_file_path)

            # Ensure the destination directory exists
            os.makedirs(dest_dir_path, exist_ok=True)
            
            if 'Copy' in command:
                success = copyFile(full_file_path, dest_file_path)
            elif 'Remove' in command:
                success = removeFile(dest_file_path)

    return success

def removeFile(dest_file_path):
    try:
        if os.path.isfile(dest_file_path) or os.path.islink(dest_file_path):
            os.remove(dest_file_path)  # Remove files and links
        elif os.path.isdir(dest_file_path):
            if not os.listdir(dest_file_path):
                shutil.rmtree(dest_file_path)  # Remove directories
        print(f"Removed: {dest_file_path}")
    except Exception as e:
        print(f"Error removing {dest_file_path}: {e}")

def removeHiddenDirectory(dest_file_path_hidden):
    try:        
        shutil.rmtree(dest_file_path_hidden)
        print(f"Directory {dest_file_path_hidden} and all its contents have been removed")
        parent_dir = os.path.dirname(dest_file_path_hidden)

        if not os.listdir(parent_dir):
            os.rmdir(parent_dir)
            print(f"Parent directory {parent_dir} was empty and has been removed")
        else:
            print(f"Parent directory {parent_dir} is not empty and was not removed")
    
    except Exception as error:
        print(f"Error: {error}. Directory {dest_file_path_hidden} could not be removed.")    

def createLogEntry(log_file, species_code, version, completion_status, gff3_status, copy_status):

    entry = {
        'ids': f"{species_code}-{version}",  # Assuming you need only one id per entry
        'duration': 'N/A',  # Initialize with 'N/A'
        'module': 'N/A',  # Initialize with 'N/A'
        'completion_status': 'SUCCESS' if completion_status else 'FAILED',
        'commandError': 'N/A',  # Initialize with 'N/A',
        'entap_usage': 'N/A',
        'gff_mismatches': 'SUCCESS' if gff3_status else 'FAILED',
        'copy_successful': 'SUCCESS' if copy_status else 'FAILED'
    }
    try:
        with open(log_file, 'r') as f:
            found_command_error = False
            for line in f:
                if extract_duration(line):
                    entry['duration'] = extract_duration(line)
                if not completion_status:
                    if extract_module(line):
                        entry['module'] = extract_module(line)
                
                    if found_command_error:
                        entry['commandError'] = line.strip()  # Capture the next line after "Command error:"
                        found_command_error = False  # Reset flag to avoid capturing multiple times
                
                    if "Command error:" in line:
                        found_command_error = True  # Set flag if "Command error:" is found

        return entry
    except FileNotFoundError:
        return "genomeLooper.out not found"

def getGff3Status(gff3check_file):
    mismatches = None
    total_ids = None

    if os.path.isfile(gff3check_file):
        with open(gff3check_file, 'r') as gfflines:
            for line in gfflines:
                # Search for mismatches
                mismatch_pattern = re.search(r"Mismatches: (\d+)", line)
                if mismatch_pattern:
                    mismatches = int(mismatch_pattern.group(1))
                
                # Search for total IDs
                total_ids_pattern = re.search(r"Total IDs: (\d+)", line)
                if total_ids_pattern:
                    total_ids = int(total_ids_pattern.group(1))

            # Now check if both mismatches and total_ids were found and make the comparison
            if mismatches is not None and total_ids is not None:
                return mismatches <= total_ids
            else:
                # If either mismatches or total_ids wasn't found, treat as a failure or handle appropriately
                return False
    else:
        # If file doesn't exist, handle as you see fit; returning True here as per original logic
        return True

def appendToDataFrame(data_to_append, csv_filename='genomeLooperSummary.csv'):
    # Convert scalar values in the dictionary to single-element lists
    data_to_append = {key: [value] for key, value in data_to_append.items()}
    
    new_data_df = pd.DataFrame(data_to_append)
    
    try:
        df = pd.read_csv(csv_filename)
        updated_df = pd.concat([df, new_data_df], ignore_index=True)
    except FileNotFoundError:
        updated_df = new_data_df
    
    updated_df.to_csv(csv_filename, index=False)

def main():
    
    if len(sys.argv) != 4 or sys.argv[1] == "-h":
        print_usage()
    
    # inputs #
    species_code = sys.argv[1]
    version = sys.argv[2]
    base_dir = sys.argv[3]
    # Directory
    src_base_dir = sys.argv[4] if len(sys.argv) == 4 else f"/isg/treegenes/treegenes_store/FTP/Genomes"
    
    # Parameters #
    # Directory
    src_dir = f"{src_base_dir}/{species_code}/{version}"
    # File
    status_file = f"genomeLooperSummary.csv"
    temp_dir = f"{base_dir}/{species_code}/{version}"
    log_file = f"{temp_dir}/.genomeLooper.out"
    gff3check_file = f"{temp_dir}/.checkGFF3matchesFASTA_summary.txt"
    list_of_files_to_not_delete = []

    # Read in .genomeLog for status
    completion_status = checkIfCompletedSuccessfully(log_file)
    # Read in .checkGFF3 for ID mismatches
    gff3Ids_status = getGff3Status(gff3check_file)

    # If pipeline completed successfully and no ID mismatches (allows partial)
    if (completion_status and gff3Ids_status) or (completion_status and not os.path.isfile(gff3check_file)):
        # creates directory at src_base_dir if not already there
        checkDirectoryExists(temp_dir, src_base_dir, f"{species_code}/{version}")
        # copies files over
        copy_status = operateOnFilesToDirectory(temp_dir, src_dir, exclude_files = set(['.nextflow.log', '.pipeline_params.yml', '.genomeLooper.err', '.checkGFF3matchesFASTA_summary.txt', '.trace.txt']), exclude_dirs = set(['.nextflow']), command = 'Copy')

    else:
        copy_status = False

    # append assembly status to log report
    items = createLogEntry(log_file, species_code, version, completion_status, gff3Ids_status, copy_status)
    print(items)
    # append info to genomeLooperSummary.csv
    appendToDataFrame(items)

if __name__ == "__main__":
    main()
