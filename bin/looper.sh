#!/usr/bin/env bash
#SBATCH --job-name=.genomeDirLooper
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 5
#SBATCH --mem=3G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

cd /isg/treegenes/treegenes_store/FTP/.genomes_temp/Gabe
Genomes="/isg/treegenes/treegenes_store/FTP/Genomes/*/*"

module load nextflow
nextflow pull TreeGenes/New_Genome_Pipeline # Check for any updates in the pipeline

create_new_dir() {
    local species_version_dir="${SPECIES_CODE}/${VERSION}"
    # if directory doesn't already exist create it
    if [ ! -d "${species_version_dir}" ]; then
        mkdir -p "${species_version_dir}"
    fi
}

create_nxf_input() {
    local file_path="${2}"
    local variable="${1}"
    if [[ -n "$file_path" ]]; then
        declare -g "NXF_${variable}"=$file_path
    else
        declare -g "NXF_${variable}"=""
    fi
}

write_nextflow_yml_input() {
    # Write input file with empty or path to files
    echo "species_code: ${SPECIES_CODE}" > .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "version: ${VERSION}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "fasta: ${NXF_FASTA}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "gff: ${NXF_GFF}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "gtf: ${NXF_GTF}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "pfasta: ${NXF_PFASTA}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "cfasta: ${NXF_CFASTA}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "tfasta: ${NXF_TFASTA}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "taxon: ${GENUS}" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "haplotype: $HAP_NUMBER" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "outdir: ./" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "create_dir: false" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "workflow: Genomes" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "checkGFF3: true" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
    echo "runNewGenomesPipeline: true" >> .${HAP_PARMS_FILENAME}pipeline_params.yml
}

create_nxf_arguments() {
    # Define file patterns and their corresponding directories
    declare -A file_patterns=( 
        ["FASTA"]="*${HAP_NUMBER}.fa *${HAP_NUMBER}.fa.gz *${HAP_NUMBER}.fasta.gz *${HAP_NUMBER}.fasta"
        ["GFF"]="*${HAP_NUMBER}.gff *${HAP_NUMBER}.gff.gz *${HAP_NUMBER}.gff3 *${HAP_NUMBER}.gff3.gz"
        ["GTF"]="*${HAP_NUMBER}.gtf *${HAP_NUMBER}.gtf.gz"
        ["PFASTA"]="*${HAP_NUMBER}.peptides.fa.gz *${HAP_NUMBER}.pep.fa.gz *${HAP_NUMBER}.pep.fa *${HAP_NUMBER}.proteins.fa.gz *${HAP_NUMBER}.proteins.fa"
        ["CFASTA"]="*${HAP_NUMBER}.cds.fa.gz *${HAP_NUMBER}.cds.fa"
        ["TFASTA"]="*${HAP_NUMBER}.transcripts.fa.gz *${HAP_NUMBER}.transcripts.fa *${HAP_NUMBER}.tsa.fa *${HAP_NUMBER}.tsa.fa.gz *${HAP_NUMBER}.cdna.fasta"
    )

    # Function to process each file type
    for file_type in "${!file_patterns[@]}"; do
        local target_dir="$genome_dir/genome"  # Default target directory
        pattern="${file_patterns[$file_type]}"
        # Convert space-separated patterns to -o (or) conditions for find command
        find_pattern=$(echo $pattern | sed 's/ / -o -name /g')
        
        # Check if file type should use NEW_ANNOTATION_DIR
        if [[ "$file_type" == "PFASTA" || "$file_type" == "CFASTA" || "$file_type" == "GFF" ]]; then
            local target_dir="$genome_dir/annotation"
        elif [[ "$file_type" == "TFASTA" ]]; then 
            local target_dir="$genome_dir/annotation"        
        fi

        files=$(find -L "$target_dir" -name $find_pattern 2>/dev/null) # use /dev/null to redirect errors to file that gets deleted
        create_nxf_input "$file_type" $files
        # create a way to copy files to new path

    done

    write_nextflow_yml_input
}

wait_for_x_jobs_to_finish() {
    local max_number_of_workflows=$1
    while :; do
        local number_of_jobs_with_sh_New_Genome=$(squeue -u gbarrett | grep -E "[A-Z]{1}[a-z]{3}-v[0-9]{1}" | awk '{print $1}' | wc -l)
        if (( number_of_jobs_with_sh_New_Genome <= max_number_of_workflows )); then
            break # Exit loop if the number of jobs is less than or equal to the limit
        else
            sleep 5m # Wait for 5 minutes before checking again
        fi
    done
}

check_ftp_matches_database_versions() {
    species_code=$1
    version=$2
    sql_filename=${3:-'/isg/treegenes/treegenes_store/FTP/.genomes_temp/species_codes_and_versions.csv'}

    if awk -F',' -v code="$species_code" -v ver="$version" '$4 == code {print; exit}' $sql_filename | grep -q .; then
        genus=$(awk -F',' -v code="$species_code" -v ver="$version" '$4 == code {print $1; exit}' "$sql_filename")
        echo $genus
    else
        echo "$species_code: $version not found in database" >> genomes_unmatched.log
        return 1
    fi
}

############################################################
# MAIN WORKFLOW
############################################################

# File populated with check fails
touch genomes_unmatched.log

# Set dotglob to also explore hidden files
shopt -s dotglob
species_code_pattern="^[A-Z]{1}[a-z]{3}$"
version_pattern="^v[0-9]\.[0-9]?[0-9]$"
haplotype_pattern="hap[0-7]{1}"

for genome_dir in $Genomes; do
    last_job_id='' # Reset for each genome assembly
    SPECIES_CODE=$(echo "$(basename "$(dirname $genome_dir)")" | sed 's/\.//')
    VERSION=$(basename $genome_dir)

    if [[ "$VERSION" =~ $version_pattern && $SPECIES_CODE =~ $species_code_pattern ]]; then
        GENUS=$(check_ftp_matches_database_versions $SPECIES_CODE $VERSION)
        ret=$?
        if [ $ret -ne 0 ]; then
            continue  # Skip to the next iteration of the loop
        fi
        
        create_new_dir
        for HAP in $genome_dir/genome/*.{fa,fa.gz}; do
            if [[ -f "$HAP" ]]; then # check if file exists since prints *.fa if no matches
                if [[ $HAP =~ ($haplotype_pattern) ]]; then
                    HAP_NUMBER="${BASH_REMATCH}"
                    HAP_PARMS_FILENAME="${BASH_REMATCH}_" # intended for writing file with '_' nonmenclature                    
                else
                    HAP_NUMBER=''
                    HAP_PARMS_FILENAME=''
                fi
                echo $HAP
                cd "./${SPECIES_CODE}/${VERSION}" # move into directory for writing files 
                create_nxf_arguments # looks for files and defaults to ''

                write_nextflow_yml_input # writes nextflow input argument yml structure file. Handles empty ''

                wait_for_x_jobs_to_finish 5 # don't run more than 5 New_Genome_Pipeline workflows
                
                # Prepare to submit the job, with dependency if applicable
                if [[ -n "$last_job_id" ]]; then
                    v=$1
                    echo "submit job $SPECIES_CODE $VERSION"
                    job_output=$(sbatch --dependency=afterok:$last_job_id --job-name "${SPECIES_CODE}-${VERSION}" ../../nf-New_Genome_Pipeline.sh $SPECIES_CODE $VERSION .${HAP_PARMS_FILENAME}pipeline_params.yml)
                else
                    t=$1
                    echo "submit job w/ no dependency $SPECIES_CODE $VERSION"
                    job_output=$(sbatch --job-name "${SPECIES_CODE}-${VERSION}" ../../nf-New_Genome_Pipeline.sh $SPECIES_CODE $VERSION .${HAP_PARMS_FILENAME}pipeline_params.yml)  # submit job in genome assembly directory. writes .nextflow.log in directory and avoids LOCK file error
                fi
                
                # Capture the new job's ID if HAP_NUMBER is not an empty string
                if [[ $job_output =~ Submitted\ batch\ job\ ([0-9]+) && -n "$HAP_NUMBER" ]]; then
                    last_job_id=${BASH_REMATCH[1]}
                fi

                cd ../../ # back out of genome assembly
                
                #
                # outside of directory scope processes
                #
                echo $SPECIES_CODE

                chown -R :treegenes ${SPECIES_CODE}/${VERSION} && chmod -R 775 ${SPECIES_CODE}/${VERSION} # switch permissions before proceeding 
                python3 writeGenomesSummary.py ${SPECIES_CODE} ${VERSION} ./


            fi
        done
    else
        echo "$genome_dir" >> ./genomes_unmatched.log # if the patterns don't match expect note in file and move onto next genome
    fi
done

