# Description on `Looper.sh`

Working with multiple genome assemblies one at a time can be exhausting. `looper.sh` was created to try and automate updating genome assemblies based on the treegenes new_genome_pipeline.

This script will:

1) check that the version and species code patterns match the correct format
2) check species code and version are contained within the database
3) create a new directory, following the same format of `<species_code>/<version>` in the current working directory
4) writes a input yml file as input into nextflow
5) submits at must 5 genome assemblies onto `Xanadu`

## Execution
to run `looper.sh` provide the base of the directory where the four letter codes are stored, like for example `/isg//isg/treegenes/treegenes_store/FTP/Genomes`. 

```
sbatch looper.sh /isg//isg/treegenes/treegenes_store/FTP/.genomes_temp/Gabe
```

## File Permissions

Ensure permissions are read and writeable for the group `treegenes`

To switch group from `user` to `treegenes`:
```
chown -R :treegenes
```

To modify the permissions for the group:
```
chmod -R g+rw,o+r /path/to/directory
```

## Output Files

### `genomes_unmatched.log`
output messages on if not found in database or if particular directories don't contain the right format.

### `.genomeDirLooper_<>_<>.err`
captures any potential error message. Usually empty.

### `.genomeDirLooper_<>_<>.out`
program output logging messages outputted to the console. 