# TreeGenes/New_Genome_Pipeline: Output

## Introduction

This document describes the output produced by the pipeline.

### .nextflow.log

This file contains the most detailed summary output and contains INFO/WARN/ERROR log output. 


---
Upon workflow completion this line will be produced in the .nextflow.log

`Sep-18 09:53:29.107 [main] DEBUG n.trace.WorkflowStatsObserver - Workflow completed > WorkflowStats[succeededCount=18; failedCount=0; ignoredCount=0; cachedC
ount=0; pendingCount=0; submittedCount=0; runningCount=0; retriesCount=0; abortedCount=0; succeedDuration=21.8s; failedDuration=0ms; cachedDuration=0ms;loadC
pus=0; loadMemory=0; peakRunning=2; peakCpus=5; peakMemory=4 GB; ]`

---

If the workflow fails NextFlow will output this structure into .nextflow.log or the SLURM .out file assigned in the SLURM Header. 

```Sep-18 02:41:37.954 [TaskFinalizer-2] DEBUG nextflow.processor.TaskProcessor - Handling unexpected condition for
  task: name=TREEGENES_BUILD_DATABASE:GENOME_DATABASE_BUILDER:FASTA_BUILDER:BWA_INDEX (Abal.1_1.fa); work-dir=/scratch/gbarrett/f5/46f450be71fc2935c9b3a0bdfd50ef
  error [nextflow.exception.ProcessFailedException]: Process `TREEGENES_BUILD_DATABASE:GENOME_DATABASE_BUILDER:FASTA_BUILDER:BWA_INDEX (Abal.1_1.fa)` terminated with an error exit status (140)
Sep-18 02:41:38.160 [TaskFinalizer-2] ERROR nextflow.processor.TaskProcessor - Error executing process > 'TREEGENES_BUILD_DATABASE:GENOME_DATABASE_BUILDER:FASTA_BUILDER:BWA_INDEX (Abal.1_1.fa)'

Caused by:
  Process `TREEGENES_BUILD_DATABASE:GENOME_DATABASE_BUILDER:FASTA_BUILDER:BWA_INDEX (Abal.1_1.fa)` terminated with an error exit status (140)


Command executed:

  bwa \
      index \
       \
      -p Abal.1_1 \
      Abal.1_1.fa

  cat <<-END_VERSIONS > versions.yml
  "TREEGENES_BUILD_DATABASE:GENOME_DATABASE_BUILDER:FASTA_BUILDER:BWA_INDEX":
      bwa: $(echo $(bwa 2>&1) | sed 's/^.*Version: //; s/Contact:.*$//')
  END_VERSIONS

Command exit status:
  140

Command output:
  (empty)

Command error:
  [BWTIncConstructFromPacked] 2560 iterations done. 25600000000 characters processed.
  [BWTIncConstructFromPacked] 2570 iterations done. 25700000000 characters processed.

Work dir:
  /scratch/gbarrett/f5/46f450be71fc2935c9b3a0bdfd50ef
```