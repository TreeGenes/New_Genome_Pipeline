@Grab(group='com.github.samtools', module='htsjdk', version='4.1.1')

import nextflow.Nextflow;
import nextflow.io.ValueObject;
import java.io.File;
import referencegenomechecks.*;
import htsjdk.tribble.gff.Gff3Codec;
import htsjdk.tribble.gff.Gff3Feature; 
import htsjdk.tribble.FeatureReader;
import htsjdk.tribble.AbstractFeatureReader;
import htsjdk.samtools.util.BlockCompressedOutputStream;
import htsjdk.samtools.reference.FastaSequenceFile;
import htsjdk.samtools.reference.ReferenceSequence;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@ValueObject
class WorkflowGFFChecks {
    static final String outputFile = ".checkGFF3matchesFASTA_summary.txt" // Accessible throughout the class

    public static void checkGFF3MatchesFASTA(log, params, gff3, List<Map<String, String>> fastaFileLinkedMapList) {
        final long start = System.currentTimeMillis()
        log.info("[checkGFF3MatchesFASTA] igniting")
        //createLogFile(outputFile)


        String gffFilePath = gff3

        Set<String> features_of_interest = ['mRNA', 'CDS', 'transcript']
        Set<String> attributes_of_interest = ['transcript_id', 'protein_id', 'ID', 'Name', 'pacid', 'GenBank']

        try {
            for (Map<String, String> fastaFileLinkedMap : fastaFileLinkedMapList) {
                for (Map.Entry<String, String> entry : fastaFileLinkedMap.entrySet()) {
                    String key = entry.getKey(); // fasta type
                    String fastaFilePath = entry.getValue(); // file path
                    
                    Summary summary = new Summary();
                    
                    Boolean isAnnotation = key == "gfasta" ? false : true;

                    File fasta = new File(fastaFilePath);

                    FastaHeaderReader reader = new FastaHeaderReader(fasta, summary, isAnnotation);

                    //log.info(headers.toString());

                    // tailor gff3 reader to parse specific features and attributes
                    Gff3Codec codec = new CustomGff3Codec(features_of_interest, attributes_of_interest);
                    FeatureReader<Gff3Feature> gffReader = AbstractFeatureReader.getFeatureReader(gffFilePath, codec, false);

                    FastaGffComparator comparator = new FastaGffComparator(reader.getModifiedHeaders(), gffReader, summary, isAnnotation);
                    comparator.runComparator(); // start the gff iterator

                    log.info(key + " " + summary.getSummaryMapString());           

                    if (summary.getTotalMismatches() > 0) {
                        log.info("Found mismatches going to initiate pattern searching in headers");
                        Map<String, Map<String, Map<String, Integer>>> gffidsMap = comparator.getUnifiedIdsMap();

                        SynchronizedFastaHeaderIterator findsubstring = new SynchronizedFastaHeaderIterator(reader.getOriginalHeaders(), gffidsMap, summary, isAnnotation);

                        // If found partial matches in header write new fasta
                        if (findsubstring.getFoundSubStringMatch()) {
                            log.info("Updating header of " + key + " to match" + summary.getSummaryMapString());
                            Map<String, String> ids = findsubstring.getUpdatedFastaIds();
                            // Get the absolute path of the directory containing the original FASTA file
                            String suffix = params.species_code + "." + params.version.replaceAll("v","").replaceAll("\\.", "_")
                            String fastaName = ""
                            String newHeaderName = ""
                            String source = params.source.toString().toLowerCase().equals('user') ? 'GCA' : 'GCF'
                            switch (key) {
                                case "gfasta":
                                    fastaName = "./" + source + "/genome/" + suffix + ".cor.fa.gz"
                                    newHeaderName = "./" + source + "/genome/" + suffix + ".tsv"
                                    break
                                case "cfasta":
                                    fastaName = "./" + source + "/annotation/" + suffix + ".cor.cds.fa.gz"
                                    newHeaderName = "./" + source + "annotation/" + suffix + ".cds.tsv"
                                    break
                                case "pfasta":
                                    fastaName = "./annotation/" + suffix + ".cor.pep.fa.gz"
                                    newHeaderName = "./annotation/" + suffix + ".pep.tsv"
                                    break
                                case "tfasta":
                                    fastaName = "./annotation/" + suffix + ".cor.transcripts.fa.gz"
                                    newHeaderName = "./annotation/" + suffix + ".transcripts.tsv"
                                    break
                            }
                            
                            String directory = fasta.getParentFile().getAbsolutePath();
                            
                            // Create the new FASTA file path using the absolute directory path
                            //String newFasta = directory + File.separator + params.species_code + "_" + params.version + ".fa.gz";
                            File newFastaFile = new File(fastaName);
                            //newFastaFile.createNewFile();

                            log.info("Found SubString Matches. Writing new Fasta " + fastaName);

                            try (FastaSequenceFile fastaReader = new FastaSequenceFile(fasta, false);
                                BlockCompressedOutputStream bcos = new BlockCompressedOutputStream(newFastaFile);
                                BufferedWriter tsvWriter = new BufferedWriter(new FileWriter(newHeaderName))) {  // Create a BufferedWriter for the TSV file

                                ReferenceSequence sequence;
                                while ((sequence = fastaReader.nextSequence()) != null) {
                                    String originalHeader = sequence.getName();
                                    String sequenceString = sequence.getBaseString();

                                    if (ids.containsKey(originalHeader)) {
                                        String updatedHeader = ids.get(originalHeader);
                                        
                                        // Write to the new FASTA file
                                        bcos.write((">" + updatedHeader + "\n").getBytes(StandardCharsets.UTF_8));
                                        bcos.write((sequenceString + "\n").getBytes(StandardCharsets.UTF_8));

                                        // Write to the TSV file
                                        tsvWriter.write("\"" + originalHeader + "\"\t\"" + updatedHeader + "\"\n");
                                    }
                                }

                                //System.out.println("FASTA file successfully written to: " + newFastaFile);

                            } catch (Exception e) {
                                log.error("Error while writing the new FASTA file or TSV file: " + newFastaFile);
                                e.printStackTrace();
                                throw new RuntimeException("Error while writing the new FASTA file or TSV file", e);
                            }

                            //FastaUpdater updater = new FastaUpdater(fasta, newFasta, findsubstring.getUpdatedFastaIds());

                            if (!newFastaFile.exists()) {
                                log.error("New FASTA file was not created: " + newFastaFile.getAbsolutePath());
                            } else {
                                log.info("New FASTA file created successfully: " + newFastaFile.getAbsolutePath());
                            }
                        }
                    }

                    // Output summary: total counts
                    //writeLogToFile(summaryTable.getSummaryForAllKeys());
                }
            }
        } catch (Exception e) {
            log.error("Exception: " + e);
            e.printStackTrace();
        }
    }
}
