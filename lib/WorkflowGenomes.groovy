import nextflow.Nextflow
import java.util.regex.Pattern
import java.util.regex.Matcher
import java.net.InetAddress
import groovy.json.JsonSlurper
import java.nio.file.Path
import java.nio.file.Files
import java.nio.file.Paths

//import groovy.sql.Sql
//@Grab('mysql:mysql-connector-java:8.0.26')
//@GrabConfig(systemClassLoader=true)
//@Grab(group='org.postgresql', module='postgresql', version='9.4-1205-jdbc42')

class WorkflowGenomes {
    static final String RESET = "\u001B[0m"
    static final String RED = "\u001B[31m"
    static final String GREEN = "\u001B[32m"
    static final String BLUE = "\u001B[34m"
    //
    // Check and validate parameters
    //
    public static void initialise(params, log) {

        if (!params.species_code) {
            log.error "must specify species_code with e.g. '--species_code Alba' or via a detectable config file."
            System.exit(1)
        }
        if (!params.version || params.version == null) {
            log.error "must specify version with e.g. '--version v1.1' or via a detectable config file."
            System.exit(1)
        } else { // this won't work because .replaceAll() gets initialized before this check
            if (!params.version ==~ /^v\d+\.\d+$/) {
                log.error "version must resemble something like this ex. 'v1.0'"
                System.exit(1)
            }
        }
        if (!params.fasta || params.fasta == null) {
            log.error "must specify fasta with e.g. '--fasta /path/to/fasta' or via a detectable config file."
            System.exit(1)
        }
        if (params.enable_entap && (params.taxon == null || !params.taxon)) {
            log.error "must specify taxon with e.g. '--taxon id' or via a detectable config file like within nextflow.config"
            System.exit(1)
        }
        if (!params.source || params.source == null) {
            log.error "must specify source with e.g. '--source <user|db?>' or via detectable config file"
            System.exit(1)
        } else {
            if (!params.source.toString().toLowerCase().equals('user') && !params.source.toString().toLowerCase().equals('db')) {
                log.error "supplied parameter to --source is not supported. Must be either 'user' or 'db'"
                System.exit(1)
            }
        }
    }

    public static void changePermissions(workflow, launchDir, log) {
        workflow.onComplete {
            def cmd = "chmod -R 775 ${launchDir}"
            def exitCode = cmd.execute().waitFor()
            if (exitCode != 0) {
                log.error "Failed to change directory permissions"
            }
        }
    }

    public static String getHostName(params, log) {
        def localHost = InetAddress.getLocalHost()
        def hostname = localHost.getHostName().toString()
        log.info(hostname)
        return hostname
    }

    public static void checkGff3MatchesFasta(jsonUnixPath, log) {
        def jsonSlurper = new JsonSlurper()

        // Convert UnixPath (Path object) to File
        def file = jsonUnixPath.toFile()

        // Parse the JSON file
        def jsonData = jsonSlurper.parse(file)
        def detected = false

        // Iterate over each file summary in the JSON data
        jsonData.each { fileSummary ->
            if (fileSummary.corrected) {
                log.warn "File '${fileSummary.filename}' contains mismatches in IDs and were successfully corrected!"
                // Exit the pipeline with a non-zero status code
            } else if (fileSummary.totalMismatches > 0 && !fileSummary.corrected) {
                detected = true
                log.warn "File '${fileSummary.filename}' contains mismatches in IDs."
            } else {
                log.info "File '${fileSummary.filename}' headers match gff."
            }
        }
        if (detected) {
            log.error "exiting due to mismatches detected." 
            System.exit(1)
        }
    }

    public static ArrayList checkQuastStatistics(params, quast_log, log, type) {
        //def min_total_length = '200000000'
        //def min_number_genes = '10000'
        //def min_n50          = '300'

        def total_length_pattern  = /^Total\s+length\s+(\d+)\s*/
        def total_genes_pattern   = /^#\s+contigs\s+(\d+)\s*/
        def n50_pattern           = /^N50\\s+(\d+)\s*/

        quast_log.eachLine { line ->
            if ( type == 'genome' ) {
                def total_length_matcher = line =~ total_length_pattern
                if (total_length_matcher) {
                    
                
                    def total_size = total_length_matcher[0][1].toFloat()
                    if (total_size < params.min_total_length.toFloat()) {
                        log.error "genome size is smaller than ${params.min_total_length}. Please check that the .fasta is correct"
                        System.exit(1)
                        }
                    }
            } else if ( type == 'cds' ) {
                def total_genes_matcher = line =~ total_genes_pattern
                if (total_genes_matcher) {
                    
                    def total_genes = total_genes_matcher[0][1].toFloat()

                    if (total_genes < params.min_number_genes.toFloat()) {
                        log.error "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n" +
                            "  Number of genes in coding sequence fasta: ${params.cfasta} is less than 10,000 \n\n" +
                            "  \n" +
                            "  File could be specified incorrectly or may be a different kind of fasta sequence other than coding\n\n" +
                            "  Amend '--failBasedOnStatistics' to change this behaviour.\n" +
                            "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
                        System.exit(1)
                        }
                    }
                def n50_matcher = line =~ n50_pattern
                if (n50_matcher) {

                    def n50 = n50_matcher[0][1].toFloat()

                    if (n50 < params.min_n50.toFloat()) {
                        log.error 'n50 in .cds.fasta is less than 300bp. Please check that the .cds.fasta is correct'
                        System.exit(1)
                    }
                }
            } else {
                log.error 'issue with type in ./workflows/genomes.nf from checkQuastStatistics '
                System.exit(1)
            }
        } 
    }
    public static ArrayList checkBuscoScore(params, busco_log, log) {
            
        def busco_score_pattern = /^\b(\d+\.\d+)%/

        def matcher = busco_log =~ busco_score_pattern
        if (matcher.find()) {
            def completeness = matcher.group(1).toFloat()

            if (completeness < params.min_busco_score.toFloat()) {
                log.error 'busco score below ' + params.min_busco_score + ' please check files'
                System.exit(1)
            }
        }
    }
}
    /*public static ArrayList genomeCodeQuery(params, log) {

        def sql = Sql.newInstance(url, user, password, driver)
        sql.execute("set search_path to chado;")

        try {
            // Execute an SQL query
            //def query = "select o.genus, o.species, oc.code, ot.plant_type from chado.organism o join (select organism_id, value as code from chado.organismprop where type_id = 52307) oc on o.organism_id = oc.organism_id join (select organism_id, value as plant_type from chado.organismprop where type_id = 49471) ot on o.organism_id = ot.organism_id where o.organism_id in (select distinct organism_id from chado.stock) order by o.genus, o.species;"
            def query = "select genus||' '||species as species from chado.organism where organism_id = (select organism_id from chado.organismprop where type_id = 52307 and value ilike '${params.species_code}');"
            def result = sql.rows(query)
            // Process the query result
            result.each { row ->
                // Access column values using column names
                def genus = row.genus
                def code = row.value

                params.taxon = genus

            }
        } catch (Exception e) {
            // Handle the exception and provide error output
            println "An error occurred while executing the SQL query:"
            println e.message 
        } finally {
            // Close the database connection
            sql.close()
        }
    }
}
        /*
        public static ArrayList writeAnnotationReadMe(params, quast, busco, log) {

            def outputFile = new File("README.md")

            def entapConfig = "#########\nQuast Results\n#########\n" + quast_file.text

            outputFile.write(combinedContent)

        }
}*/
