#!/usr/bin/env nextflow
include { GENOME_DATABASE_BUILDER } from './workflows/genomes.nf'
/*
========================================================================================
    treegenes/genome_database
========================================================================================
    Github : https://
    Website: https://
    Slack  : https://
----------------------------------------------------------------------------------------
*/

nextflow.enable.dsl = 2

// initialize parameters


// main pipeline
workflow TREEGENES_BUILD_DATABASE {
    
    GENOME_DATABASE_BUILDER ()

}

// default WORKFLOW
workflow {

    TREEGENES_BUILD_DATABASE ()

}